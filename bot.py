from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.common.exceptions import NoSuchElementException
import getpass
import sys
import time

print("Welcome to Arvin's simple MyADZU Bot")

userNameValue = input("Enter Username: ")
passwordValue = getpass.getpass("Enter Password: ")
operationsList = "[1]: DTR [2]: Leave Credit [3]: PaySlip "
print(operationsList)

operations = int(input("Select Operation: "))

if userNameValue != '' or passwordValue:
    options = webdriver.ChromeOptions()
    options.add_argument("--start-maximized")

    driver = webdriver.Chrome(options=options)
    driver.get('https://my.adzu.edu.ph/login/index_new.php')  

    userField = driver.find_element_by_xpath("//input[@id='Username' or @name='Username']")
    userField.send_keys(userNameValue)
    
    passwordField = driver.find_element_by_xpath("//input[@name='Password']")
    passwordField.send_keys(passwordValue)
    
    loginButton = driver.find_element_by_xpath("//button[@name='Submit']")
    loginButton.click()

    if operations == 1:
        dtr = driver.find_element_by_xpath("//a[@href='https://my.adzu.edu.ph/dtr.php']")
        dtr.click()
    elif operations == 2:
        leaveCredit = driver.find_element_by_xpath("//a[@href='https://my.adzu.edu.ph/leave/index.php']")
        leaveCredit.click()  
    else:
        payslip = driver.find_element_by_xpath("//a[@href='https://my.adzu.edu.ph/payslip.php']")
        payslip.click()   
else:
    print('username and password required')